import axios from "axios";

const URL = "https://weathertjorgeapp.herokuapp.com/v1";

//Obtiene el clima actual de la ciudad
//Pre: ciudad
//Post: si no se pasa la ciudad devuelve el clima de la ubicación actual
//sino el clima de la cidada pasada, Si la ciudad no se encuentra
//devuelve error
export async function getCurrentWeather(city) {
  try {
    let response = {};
    if (city) {
      response = await axios.get(URL + "/current/" + city);
    } else {
      response = await axios.get(URL + "/current");
    }

    return response.data;
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
}
//Obtiene el pronostico de clima de la ciudad para los siguintes 5 dias
//Pre: ciudad
//Post: si no se pasa la ciudad devuelve el pronostico del clima de la ubicación actual
//sino el clima  de la ciudad pasada, Si la ciudad no se encuentra
//devuelve error

export async function getForcastFiveDays(city) {
  try {
    let response = {};
    if (city) {
      response = await axios.get(URL + "/forecast/" + city);
    } else {
      response = await axios.get(URL + "/forecast");
    }
    return response.data;
  } catch (e) {
    throw new Error(e);
  }
}
