/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Home from "../../pages/index";

describe("Home", () => {
  it("Debe renderizar Weather App y el boton Search", () => {
    const textToFind1 = "Weather App";
    const textToFind2 = "Search";
    act(() => {
      render(<Home />);
    });
    const heading = screen.getByText(textToFind1);
    const button = screen.getByText(textToFind2);
    expect(heading).toBeInTheDocument();
    expect(button).toBeInTheDocument();
  });
});
