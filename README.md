# Enunciado
Frontend Test
Preferentemente desarrollar en React.js, como alternativa React Native (sin expo).
Desarrollar una app full client-side que permita visualizar el pronóstico climático actual y de los
próximos 5 días en la ubicación actual y permita visualizar el pronóstico de otras 5 ciudades
seleccionables.
Se debe idear y diseñar la UI que se considere mas acorde (No se valoran habilidades de diseño, sino de
uso de componentes).
Los datos deben ser consumidos de la API desarrollada (en caso de que la prueba lo requiera) o la API
externa (Si solo se evalúa Front). 

# Estructura del projecto
    -pages
        -_app.js
        -index.js
    -components
        -SearchBar.js
        -WeatherCard.js
        -WeatherForecast.js    
    -public
        -lluvia.jfif
        -nublado.jfif
        -sun.jfif
   -styles
        -global.css
        -home.module.css
   -weatherApi
        -weather.js

# instalacion
npm install

# test
npm test

# run
## run dev
    npm run dev
        Puerto default:3000
## run build
   npm run build
   npm run start      
        Puerto default:3000
       
    
