import React, { useState, useEffect } from "react";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import Grid from "@material-ui/core/Grid";
import { getCurrentWeather, getForcastFiveDays } from "../weatherApi/weather";
import SearchBar from "../components/SeachBar";
import WeatherForecast from "../components/WeatherForecast";

/* 
  Componente funcional que renderiza la pagina inicial de la app
*/
export default function Home() {
  const [currentWeather, setcurrentWeather] = useState({});
  const [forecastWeather, setforecastWeather] = useState([]);
  const [cities, setCities] = useState([]);
  const [ciudadActual, setCiudadActual] = useState("");

  //Agrego la cidad de la ubicación actual
  useEffect(() => {
    handleAddCity();
  }, []);

  /* Funcion que agrega una ciudad , su clima actual y su pronostico
  para los siguientes 5 días */
  const handleAddCity = async (cityToAdd) => {
    getCurrentWeather(cityToAdd)
      .then((weather) => {
        let current = currentWeather;
        current[weather.city] = weather;
        setcurrentWeather(current);
        if (ciudadActual == "") {
          setCiudadActual(weather.city);
        }
        getForcastFiveDays(cityToAdd)
          .then((forecast) => {
            if (cities.find((city) => city == forecast.city)) {
              alert("La ciudad ya está seleccionada");
            } else {
              setforecastWeather((old) => [...old, forecast]);
              setCities((old) => [...old, forecast.city]);
            }
          })
          .catch((e) => {
            console.log(e);
            alert("Error la ciudad " + cityToAdd + " no existe o hubo un error de conexion");
          });
      })
      .catch((e) => {
        console.log(e);
        alert("Error la ciudad " + cityToAdd + " no existe o hubo un error de conexions");
      });
  };
  /* Funcion que elimina una ciudad , su clima actual y su pronostico
  para los siguientes 5 días */
  const handleRemoveCity = (cityToRemove) => {
    let newForcast = forecastWeather.filter((data) => data.city != cityToRemove);
    let newCities = cities.filter((city) => city != cityToRemove);
    let newCurrent = currentWeather;
    delete newCurrent[cityToRemove];
    setCities(newCities);
    setcurrentWeather(newCurrent);
    setforecastWeather(newForcast);
  };

  return (
    <Grid
      container
      spacing={10}
      style={{
        display: "flex",
        alignItems: "center",
        background: "linear-gradient(to right, #E040FB, #00BCD4)"
      }}
    >
      <Head>
        <title>Weather app</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Grid item xs={12} sm={12}>
        <h1 className={styles.title}>Weather App</h1>
        <h4 style={{ textAlign: "center" }}>{"Ubicación: " + ciudadActual}</h4>
      </Grid>
      <Grid item xs={12} sm={12}>
        <SearchBar cities={cities} addCityWeather={handleAddCity} removeCityWeather={handleRemoveCity} />
      </Grid>
      <Grid item xs={12} sm={12}>
        {forecastWeather.length > 0 && <WeatherForecast current={currentWeather} forecast={forecastWeather} />}
      </Grid>

      <Grid item xs={12} sm={12}>
        <footer className={styles.footer}>
          <span className={styles.logo}>Powered by @Jorge</span>
        </footer>
      </Grid>
    </Grid>
  );
}
