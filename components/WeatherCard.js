import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  dataArea: {
    backgroundColor: "#26ff"
  },
  data: {
    textAlign: "center"
  }
});

/* Componente funcional que renderiza una tarjeta con una imagen 
props: weatherData:{ date, weather, temp, humidity }
*/
export default function WeatherCard({ weatherData }) {
  const { date, weather, temp, humidity } = weatherData;
  const classes = useStyles();
  //Funcion que devuelve una imagen dependiendo del tipo de clima
  const getImagen = (weatherType) => {
    let image = "";
    switch (weatherType) {
      case "Clouds":
        image = "/nublado.jfif";
        break;
      case "Rain":
        image = "/lluvia.jfif";
        break;
      case "Clear":
        image = "/sun.jfif";
        break;
      default:
        image = "/sun.jfif";
        break;
    }
    return image;
  };
  const imagen = getImagen(weather);

  return (
    <Card sx={{ maxWidth: 300 }}>
      <CardActionArea style={{ backgroundColor: "#6179c2" }}>
        <CardMedia component="img" height="300" image={imagen || "/sun.jfif"} alt="green iguana" />
        <CardContent>
          <Typography className={classes.data} gutterBottom variant="h5" component="div">
            {date}
          </Typography>
          <Typography className={classes.data} gutterBottom variant="h5" component="div">
            {weather}
          </Typography>
          <Typography className={classes.data} gutterBottom variant="h5" component="div">
            {temp} °C
          </Typography>
          <Typography className={classes.data} gutterBottom variant="h5" component="div">
            {humidity} %
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
