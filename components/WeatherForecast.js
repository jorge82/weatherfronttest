import React, { useState, useEffect } from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import WeatherCard from "./WeatherCard";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

/* Componente funcional que renderiza una tarjeta con una imagen del clima
por el clima actual y el pronostico de los 5 dias siguientes 
props: current, forecast
*/
export default function WeatherForecast({ current, forecast }) {
  const classes = useStyles();
  return (
    <Container>
      <Grid className={classes.forecast} flexDirection="row" container spacing={2}>
        {forecast.map((cities, index) => {
          return cities.data.map((data, index) => {
            return (
              <>
                {index == 0 && (
                  <>
                    <Grid item xs={12} sm={12} key={cities.city + index}>
                      <hr />
                      <h1 className={classes.title}>{cities.city}</h1>
                      <hr />
                    </Grid>
                    {current[cities.city] && (
                      <Grid item xs={2} sm={2} key={current[cities.city] + index}>
                        <h1 className={classes.title}>Hoy</h1>
                        <WeatherCard weatherData={current[cities.city]} />
                      </Grid>
                    )}
                  </>
                )}
                <Grid item xs={2} sm={2} key={cities.city + data.date}>
                  <h1 className={classes.title}>{data.date == new Date(Date.now()).toLocaleDateString() ? "Hoy" : data.day}</h1>
                  <WeatherCard weatherData={data} />
                </Grid>
              </>
            );
          });
        })}
      </Grid>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  forecast: {
    marginTop: theme.spacing(2),
    alignItems: "center",
    marginLeft: theme.spacing(2)
  },
  title: {
    textAlign: "center",
    color: "#000000"
  }
}));
