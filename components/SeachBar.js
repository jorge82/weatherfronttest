import * as React from "react";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import Grid from "@material-ui/core/Grid";
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";

//numero de ciudades maxima: ciudad actual más 5
const MAX_CITIES = 6;

/* Componente funcional que renderiza un cuadro de búsqueda de ciudades
props: cities, addCityWeather, removeCityWeather

*/
export default function SearchBar({ cities, addCityWeather, removeCityWeather }) {
  const [search, setSearch] = React.useState("");

  const handleSearch = () => {
    if (search.length < 1) {
      alert("Por favor ingrese una ciudad");
    } else if (cities.length < MAX_CITIES && validateNotRepeated(search)) {
      addCityWeather(search);
      setSearch("");
      console.log("cities", cities);
    } else {
      alert("Solo se permiten " + MAX_CITIES + " ciudades distintas");
    }
  };

  const validateNotRepeated = (name) => {
    if (cities.find((city) => city.toLowerCase() == name.toLowerCase())) {
      return false;
    } else {
      return true;
    }
  };
  const removeCity = (city) => {
    if (confirm("Esta seguro de querer eliminar el pronostico de " + city)) {
      removeCityWeather(city);
    }
  };

  const handleKey = (e) => {
    if (e.charCode == 13) {
      handleSearch();
    }
  };
  return (
    <Grid container flexdirection={"row"}>
      <Grid item xs={12} sm={12} style={{ textAlign: "center" }}>
        <TextField
          name="city"
          justify="center"
          style={{ marginRight: 20, justifyContent: "center" }}
          value={search}
          onKeyPress={handleKey}
          onChange={(event) => setSearch(event.target.value)}
          label="Search place..."
        />
        <Button variant="contained" color="primary" onClick={handleSearch}>
          Search
        </Button>
      </Grid>
      <Grid item xs={12}>
        <Stack spacing={1} alignItems="center">
          <Stack direction="row" spacing={1}>
            {cities.map((name) => (
              <Chip key={name} label={name} color="primary" onClick={() => removeCity(name)} />
            ))}
          </Stack>
        </Stack>
      </Grid>
    </Grid>
  );
}
